import java.util.Scanner;

public class P5 {

    private Scanner sc = new Scanner(System.in);

    public void showMenu() {
        System.out.println("Escoge una opcion:");
        System.out.println("1.Hello World");
        System.out.println("2.findMax");
        System.out.println("4.average");
        System.out.println("0. Salir\n");
    }

    public int readOpcion() {
        return sc.nextInt();
    }

    public void operar(int opcion) {

        switch (opcion) {
            case 0:
                exit();
                break;
            case 1:
                helloWorld();
                break;
            case 2:
                findMax_ncc();
                break;
            case 4:
                average_ncc();
                break;
        }
    }

    /* OPERATIONS */
    public void exit() {
        System.out.println("END");
    }

    public void helloWorld() {
        System.out.println("Hello World");
    }

    public void findMax_ncc() {
        System.out.println("Introduir valors acabant amb un 0 i et retornaré el nombre més gran");

        int max = Integer.MIN_VALUE;
        int n = sc.nextInt();

        while (n != 0) {
            if (n > max) max = n;
            n = sc.nextInt();
        }

        System.out.println("El nombre introduït més gran és el " + max);
    }

    public void average_ncc() {
        System.out.println("Introduir valors acabant amb un 0 i et retornaré la mitjana dels valors introduïts");

        int suma = 0;
        int nIntroduits = 0;
        int n = sc.nextInt();

        while (n != 0) {
            suma += n;
            nIntroduits++;

            n = sc.nextInt();
        }

        System.out.println("La mitjana dels valors introduïts és: " + (double)(suma / nIntroduits));
    }

    public static void main(String[] args) {

        P5 p5 = new P5();

        int response = 0;
        do {
            p5.showMenu();
            response = p5.readOpcion();
            p5.operar(response);
        } while (response != 0);

    }
}

